from dalle2_pytorch import DALLE2

dalle2 = DALLE2(
    prior = diffusion_prior,
    decoder = decoder
)

texts = ['glistening morning dew on a flower petal']
images = dalle2(texts) # (1, 3, 256, 256)